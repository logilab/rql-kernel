from ipykernel.kernelbase import Kernel
from cwclientlib import cwproxy_for
import subprocess
import time


class RQLKernel(Kernel):
    implementation = 'rql'
    implementation_version = '1.0'
    language = 'no-op'
    language_version = '0.1'
    language_info = {
        'name': 'RQL',
        'mimetype': 'text/rql',
        'file_extension': '.rql',
    }
    banner = "RQL kernel"
    client = cwproxy_for('training')

    def do_execute(self, code, silent, store_history=True,
                   user_expressions=None, allow_stdin=False):
        if not silent:
            try:
                clean_code = ""
                for line in code.split("\n"):  # remove comments
                    clean_code += line.split("#")[0]
                if clean_code:
                    response = str(self.client.rql(clean_code).json())
                else:
                    response = str('Empty request')
            except Exception:
                response = "No cubicweb instance available"
            stream_content = {'name': 'stdout', 'text': response}
            self.send_response(self.iopub_socket, 'stream', stream_content)

        return {
            'status': 'ok',
            'execution_count': self.execution_count,
            'payload': [],
            'user_expressions': {},
        }
