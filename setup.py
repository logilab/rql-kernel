from distutils.core import setup

with open('README.rst') as f:
    readme = f.read()

setup(
    name='rql_kernel',
    version='0.1',
    packages=['rql_kernel'],
    description='RQL kernel for Jupyter',
    long_description=readme,
    author='Logilab',
    author_email='ogiorgis@logilab.fr',
    install_requires=[
        'jupyter_client', 'IPython', 'ipykernel'
    ],
    classifiers=[
        'Intended Audience :: Developers',
        'Programming Language :: Python :: 3',
    ],
)
