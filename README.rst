Kernel Installation
-------------------

> git clone https://gitlab.com/logilab/rql-kernel
> cd rql-kernel
> pip install .
> python -m rql_kernel.install


Syntax highlight installation
-----------------------------

> npm install
> jupyter labextension install .
